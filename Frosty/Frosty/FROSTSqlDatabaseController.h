//
//  FROSTSqlDatabaseController.h
//  Frosty
//
//  Created by Marcel Stüttgen and Björn Lambertz on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FROSTOrderDataController.h"

@interface FROSTSqlDatabaseController : NSObject

-(void) createDataBase;
-(void) fillDataBaseWithTestData;
-(void) writeAllData:(FROSTOrderDataController *)orderController;
-(void) dropAllData;
-(void) readAllData:(FROSTOrderDataController *)orderController;


@end
