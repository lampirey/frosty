//
//  FROSTDetailViewController.m
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FROSTDetailViewController.h"
#import "FROSTOrder.h"

@interface FROSTDetailViewController ()
- (void)configureView;
@end

@implementation FROSTDetailViewController

@synthesize order = _order,checkpoint1Label = _checkpoint1Label, checkpoint1StateSwitch = _checkpoint1StateSwitch, checkpoint2Label = _checkpoint2Label, checkpoint2StateSwitch = _checkpoint2StateSwitch, checkpoint3Label = _checkpoint3Label, checkpoint3StateSwitch = _checkpoint3StateSwitch, measurement1Label = _measurement1Label, measurement1StateLabel = _measurement1StateLabel, measurement2Label = _measurement2Label, measurement2StateLabel = _measurement2StateLabel, commentTextView = _commentTextView, measurement1UnitLabel = _measurement1UnitLabel, measurement1ValueLabel = _measurement1ValueLabel, measurement2UnitLabel = _measurement2UnitLabel, measurement2ValueLabel = _measurement2ValueLabel;

- (IBAction)textViewDoneEditing:(id)sender {
    self.order.comment = self.commentTextView.text;
    self.order.measurement1Value = self.measurement1ValueLabel.text;
     self.order.measurement2Value = self.measurement2ValueLabel.text;
    [sender resignFirstResponder];
}

- (IBAction)checked:(id)sender {
    self.order.checkpoint1State = self.checkpoint1StateSwitch.on;
    self.order.checkpoint2State = self.checkpoint2StateSwitch.on;
    self.order.checkpoint3State = self.checkpoint3StateSwitch.on;
    
    [sender resignFirstResponder];
}
//some git test2

- (IBAction)backgroundTap:(id)sender {
    [_commentTextView resignFirstResponder];
    
}


#pragma mark - Managing the detail item

- (void)setOrder:(FROSTOrder *)newOrder
{
    if (_order != newOrder) {
        _order = newOrder;
        
        // Update the view.
        [self configureView];
    }
}




- (void)configureView
{
    // Update the user interface for the detail item.
    FROSTOrder *theOrder = self.order;
    
    if(theOrder){
        
        self.commentTextView.text = theOrder.comment;
        self.checkpoint1Label.text = theOrder.checkpoint1;
        self.checkpoint2Label.text = theOrder.checkpoint2;
        self.checkpoint3Label.text = theOrder.checkpoint3;
        self.measurement1Label.text = theOrder.measurement1;
        self.measurement2Label.text = theOrder.measurement2;
        self.measurement1ValueLabel.text = theOrder.measurement1Value;
        self.measurement2ValueLabel.text = theOrder.measurement2Value;
        self.measurement2UnitLabel.text = theOrder.measurement2Unit;
        self.measurement1UnitLabel.text = theOrder.measurement1Unit;
        self.checkpoint3StateSwitch.on = theOrder.checkpoint3State;
        self.checkpoint2StateSwitch.on = theOrder.checkpoint2State;
        self.checkpoint1StateSwitch.on = theOrder.checkpoint1State;
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    self.order = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
