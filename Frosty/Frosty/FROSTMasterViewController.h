//
//  FROSTMasterViewController.h
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FROSTOrderDataController;

@interface FROSTMasterViewController : UITableViewController
@property (strong,nonatomic) FROSTOrderDataController *dataController;

@end
