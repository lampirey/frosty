//
//  FROSTOrderDataController.m
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FROSTOrderDataController.h"
#import "FROSTOrder.h"
#import "FROSTSqlDatabaseController.h"

@interface FROSTOrderDataController ()
- (void)initializeDefaultDataList;
@end

@implementation FROSTOrderDataController

@synthesize masterOrderList = _masterOrderList;

-(void) initializeDefaultDataList {
    
    //create instance of db controller and open the DB
    FROSTSqlDatabaseController *theInstance = [[FROSTSqlDatabaseController alloc] init];
    [theInstance createDataBase];
    [theInstance fillDataBaseWithTestData];

    
    NSMutableArray *orderList = [[NSMutableArray alloc] init];
    self.masterOrderList = orderList;
    
    [self addOrderWithID:@"Auftrag 1" 
                 comment:@"Schön"
               orderState:FALSE 
                    date:@"23.05.2012" 
             checkpoint1:@"Klimaanlage" 
        checkpoint1State:FALSE 
             checkpoint2:@"Türschloss" 
        checkpoint2State:FALSE 
             checkpoint3:@"Notlicht" 
        checkpoint3State:false 
            measurement1:@"Stromzähler" 
       measurement1State:FALSE 
        measurement1Unit:@"WattStunden" measurement1Value:@"12" 
            measurement2:@"Temperatur" 
       measurement2State:FALSE 
        measurement2Unit:@"Fahrenheit" 
       measurement2Value:@"200"];
    
    [self addOrderWithID:@"Auftrag 2" 
                 comment:@"Perfekt"
              orderState:FALSE 
                    date:@"23.04.2012" 
             checkpoint1:@"Klimaanlage" 
        checkpoint1State:FALSE 
             checkpoint2:@"Türschloss" 
        checkpoint2State:FALSE 
             checkpoint3:@"Notlicht" 
        checkpoint3State:FALSE 
            measurement1:@"Stromzähler" 
       measurement1State:FALSE 
        measurement1Unit:@"WattStunden" measurement1Value:@"12" 
            measurement2:@"Temperatur" 
       measurement2State:FALSE 
        measurement2Unit:@"Fahrenheit" 
       measurement2Value:@"200"];
    
    [self addOrderWithID:@"Auftrag 3" 
                 comment:@"Fast Fertig"
              orderState:FALSE 
                    date:@"23.05.2012" 
             checkpoint1:@"Klimaanlage" 
        checkpoint1State:FALSE 
             checkpoint2:@"Türschloss" 
        checkpoint2State:FALSE 
             checkpoint3:@"Notlicht" 
        checkpoint3State:FALSE 
            measurement1:@"Stromzähler" 
       measurement1State:FALSE 
        measurement1Unit:@"WattStunden" measurement1Value:@"12" 
            measurement2:@"Temperatur" 
       measurement2State:FALSE 
        measurement2Unit:@"Fahrenheit" 
       measurement2Value:@"200"];

    
}
- (void)setMasterOrderList:(NSMutableArray *)masterOrderList {
    if (_masterOrderList != masterOrderList) {
        _masterOrderList = [masterOrderList mutableCopy];
    }
}

- (id)init {
    if (self = [super init]) {
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}
- (NSUInteger)countOfList {
    return [self.masterOrderList count];
}

- (FROSTOrder *)objectInListAtIndex:(NSUInteger)theIndex {
    return [self.masterOrderList objectAtIndex:theIndex];
}

-(void)addOrderWithID:(NSString *)orderID 
              comment:(NSString *)comment 
            orderState:(BOOL)orderState 
                 date:(NSString *)date 
          checkpoint1:(NSString *)checkpoint1 
     checkpoint1State:(BOOL)checkpoint1State 
          checkpoint2:(NSString *)checkpoint2 
             checkpoint2State:(BOOL)checkpoint2State 
          checkpoint3:(NSString *)checkpoint3 
     checkpoint3State:(BOOL)checkpoint3State 
         measurement1:(NSString *)measurement1 
    measurement1State:(BOOL)measurement1State 
     measurement1Unit:(NSString *)measurement1Unit 
    measurement1Value:(NSString *)measurement1Value 
         measurement2:(NSString *)measurement2 
    measurement2State:(BOOL)measurement2State 
     measurement2Unit:(NSString *)measurement2Unit 
    measurement2Value:(NSString *)measurement2Value
{
    FROSTOrder *order;
    
    order = [[FROSTOrder alloc] initWithorderID:orderID 
                                        comment:comment 
                                      orderState:orderState 
                                           date:date 
                                    checkpoint1:checkpoint1
                               checkpoint1State:checkpoint1State
                                    checkpoint2:checkpoint2
                               checkpoint2State:checkpoint2State
                                    checkpoint3:checkpoint3
                               checkpoint3State:checkpoint3State
                                   measurement1:measurement1
                              measurement1State:measurement1State
                               measurement1Unit:measurement1Unit
                              measurement1Value:measurement1Value
                                   measurement2:measurement2
                              measurement2State:measurement2State
                               measurement2Unit:measurement2Unit
                              measurement2Value:measurement2Value];
    [self.masterOrderList addObject:order];

}


@end
