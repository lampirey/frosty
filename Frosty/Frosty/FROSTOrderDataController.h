//
//  FROSTOrderDataController.h
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FROSTOrder;

@interface FROSTOrderDataController : NSObject

@property (nonatomic, copy) NSMutableArray *masterOrderList;

- (NSUInteger)countOfList;
- (FROSTOrder *)objectInListAtIndex:(NSUInteger)theIndex;


-(void)addOrderWithID:(NSString *) orderID 
              comment:(NSString *)comment
           orderState:(BOOL)orderState 
                 date:(NSString*)date 
          checkpoint1:(NSString *)checkpoint1 
     checkpoint1State:(BOOL)checkpoint1State 
          checkpoint2:(NSString *)checkpoint2 
     checkpoint2State:(BOOL)checkpoint2State 
          checkpoint3:(NSString *)checkpoint3      
     checkpoint3State:(BOOL)checkpoint3State 
         measurement1:(NSString *)measurement1 
    measurement1State:(BOOL)measurement1State 
     measurement1Unit:(NSString *)measurement1Unit 
    measurement1Value:(NSString *)measurement1Value 
         measurement2:(NSString *)measurement2 
    measurement2State:(BOOL)measurement2State 
     measurement2Unit:(NSString *)measurement2Unit 
    measurement2Value:(NSString *)measurement2Value;


@end
