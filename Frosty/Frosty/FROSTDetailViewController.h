//
//  FROSTDetailViewController.h
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FROSTOrder;

@interface FROSTDetailViewController : UITableViewController

@property (strong, nonatomic) FROSTOrder *order;
@property (weak, nonatomic) IBOutlet UITextField *commentTextView;

@property (weak,nonatomic) IBOutlet UILabel *checkpoint1Label;
@property (weak,nonatomic) IBOutlet UISwitch *checkpoint1StateSwitch;

@property (weak,nonatomic) IBOutlet UILabel *checkpoint2Label;
@property (weak,nonatomic) IBOutlet UISwitch *checkpoint2StateSwitch;

@property (weak,nonatomic) IBOutlet UILabel *checkpoint3Label;
@property (weak,nonatomic) IBOutlet UISwitch *checkpoint3StateSwitch;

@property (weak,nonatomic) IBOutlet UILabel *measurement1Label;
@property (weak,nonatomic) IBOutlet UILabel *measurement1StateLabel;
@property (weak,nonatomic) IBOutlet UILabel *measurement1UnitLabel;
@property (weak,nonatomic) IBOutlet UITextField *measurement1ValueLabel;




@property (weak,nonatomic) IBOutlet UILabel *measurement2Label;
@property (weak,nonatomic) IBOutlet UILabel *measurement2StateLabel;
@property (weak,nonatomic) IBOutlet UILabel *measurement2UnitLabel;
@property (weak,nonatomic) IBOutlet UITextField *measurement2ValueLabel;

- (IBAction)textViewDoneEditing:(id)sender;
- (IBAction)checked:(id)sender;

@end
