//
//  FROSTOrder.m
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FROSTOrder.h"

@implementation FROSTOrder

@synthesize orderId = _orderId;
@synthesize comment = _comment;
@synthesize orderState = _orderState;
@synthesize date = _date;

@synthesize checkpoint1 = _checkpoint1;
@synthesize checkpoint1State = _checkpoint1State;
@synthesize checkpoint2 = _checkpoint2;
@synthesize checkpoint2State = _checkpoint2State;
@synthesize checkpoint3 = _checkpoint3;
@synthesize checkpoint3State = _checkpoint3State;

@synthesize measurement1 = _measurement1;
@synthesize measurement1State = _measurement1State;
@synthesize measurement1Unit = _measurement1Unit;
@synthesize measurement1Value = _measurement1Value;

@synthesize measurement2 = _measurement2;
@synthesize measurement2State = _measurement2State;
@synthesize measurement2Unit = _measurement2Unit;
@synthesize measurement2Value = _measurement2Value;

-(id) initWithorderID:(NSString *)orderID 
              comment:(NSString *)comment 
           orderState:(BOOL)orderState 
                 date:(NSString *)date 
          checkpoint1:(NSString *)checkpoint1 
     checkpoint1State:(BOOL)checkpoint1State 
          checkpoint2:(NSString *)checkpoint2 
     checkpoint2State:(BOOL)checkpoint2State 
          checkpoint3:(NSString *)checkpoint3 
     checkpoint3State:(BOOL)checkpoint3State
         measurement1:(NSString *)measurement1 
    measurement1State:(BOOL ) measurement1State 
     measurement1Unit:(NSString *)measurement1Unit measurement1Value:(NSString *)measurement1Value measurement2:(NSString *)measurement2 
    measurement2State:(BOOL)measurement2State 
     measurement2Unit:(NSString *)measurement2Unit measurement2Value:(NSString *)measurement2Value
{
    self = [super init];
    
    if (self) {
    
        _orderId = orderID;
        _comment = comment;
        _orderState = orderState;
        _date = date;
        
        _checkpoint1 = checkpoint1;
        _checkpoint1State = checkpoint1State;
        _checkpoint2 = checkpoint2;
        _checkpoint2State = checkpoint2State;
        _checkpoint3 = checkpoint3;
        _checkpoint3State = checkpoint3State;
        
        _measurement1 = measurement1;
        _measurement1State = measurement1State;
        _measurement1Unit = measurement1Unit;
        _measurement1Value = measurement1Value;
        
        _measurement2 = measurement2;
        _measurement2State = measurement2State;
        _measurement2Unit = measurement2Unit;
        _measurement2Value = measurement2Value;
        
        return self;
    }
    return nil;
}

@end
