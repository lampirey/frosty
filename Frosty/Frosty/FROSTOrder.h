//
//  FROSTOrder.h
//  Frosty
//
//  Created by Stefan Lambertz on 27.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FROSTOrder : NSObject
    @property (nonatomic, copy) NSString * orderId;
    @property (nonatomic, copy) NSString *comment;
    @property (nonatomic, assign) BOOL orderState;
    @property (nonatomic, copy) NSString *date;
    
    @property (nonatomic, copy) NSString *checkpoint1;
    @property (nonatomic, assign) BOOL checkpoint1State;

    @property (nonatomic, copy) NSString *checkpoint2;
    @property (nonatomic, assign) BOOL checkpoint2State;

    @property (nonatomic, copy) NSString *checkpoint3;
    @property (nonatomic, assign) BOOL checkpoint3State;

    @property (nonatomic, copy) NSString *measurement1;
    @property (nonatomic, assign) BOOL measurement1State;
    @property (nonatomic, copy) NSString *measurement1Unit;
    @property (nonatomic, copy) NSString *measurement1Value;

    @property (nonatomic, copy) NSString *measurement2;
    @property (nonatomic, assign) BOOL measurement2State;
    @property (nonatomic, copy) NSString *measurement2Unit;
    @property (nonatomic, copy) NSString *measurement2Value;

-(id)initWithorderID:(NSString *) orderID 
             comment:(NSString *)comment 
           orderState:(BOOL)orderState 
                date:(NSString*)date 
         
         checkpoint1:(NSString *)checkpoint1 
    checkpoint1State:(BOOL)checkpoint1State 
         checkpoint2:(NSString *)checkpoint2 
    checkpoint2State:(BOOL)checkpoint2State 
         checkpoint3:(NSString *)checkpoint3 
    checkpoint3State:(BOOL)checkpoint3State
         measurement1:(NSString *)measurement1 
   measurement1State:(BOOL)measurement1State 
    measurement1Unit:(NSString *)measurement1Unit measurement1Value:(NSString *)measurement1Value 
        measurement2:(NSString *)measurement2 
   measurement2State:(BOOL)measurement2State 
    measurement2Unit:(NSString *)measurement2Unit measurement2Value:(NSString *)measurement2Value;

@end
