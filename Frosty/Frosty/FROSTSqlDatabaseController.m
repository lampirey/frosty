//
//  FROSTSqlDatabaseController.m
//  Frosty
//
//  Created by Marcel Stüttgen on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FROSTSqlDatabaseController.h"
#import <sqlite3.h>

#define kFilename @"data.sqlite3"

@implementation FROSTSqlDatabaseController

- (NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
            NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:kFilename];
}


-(void) createDataBase {
    
    sqlite3 *database;
    
    if (sqlite3_open([[self dataFilePath] UTF8String], &database)
        != SQLITE_OK) {
        
        sqlite3_close(database);
        NSAssert(0, @"Failed to open database");
    }

    char *errorMsg;
    
    NSString *createTableCheckpoint = @"CREATE TABLE IF NOT EXISTS CheckPoint (CheckPointID integer NOT NULL PRIMARY KEY AUTOINCREMENT DEFAULT 0,OrderID integer NOT NULL,Description text NOT NULL,State boolean)";
    
    NSString *createTableMeasurement = @"CREATE TABLE IF NOT EXISTS Measurement (Value double,Unit text,Description text,MeasurementID integer NOT NULL PRIMARY KEY AUTOINCREMENT DEFAULT 0,CheckPointID integer NOT NULL)";
    
    NSString *createTableOrder = @"CREATE TABLE IF NOT EXISTS MyOrder (OrderDate timestamp NOT NULL,OrderID integer NOT NULL PRIMARY KEY AUTOINCREMENT DEFAULT 0,State boolean,Comment text)";
   
    
    if (sqlite3_exec(database, [createTableOrder UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"Failed to create table: %s", errorMsg);
    }
    
    if (sqlite3_exec(database, [createTableCheckpoint UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"Failed to create table: %s", errorMsg);
    }
    
    
    if (sqlite3_exec(database, [createTableMeasurement UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"Failed to create table: %s", errorMsg);
    }
    
}
    
-(void) fillDataBaseWithTestData {

    sqlite3 *database;
    
    if (sqlite3_open([[self dataFilePath] UTF8String], &database)
        != SQLITE_OK) {
        
        sqlite3_close(database);
        NSAssert(0, @"Failed to open database");
    }
    
    
    //check if there is already any data in the database
    
    const char *getOrderList = "SELECT * FROM MyOrder";

//    const char *sql = "select coffeeID, coffeeName from coffee";
    sqlite3_stmt *selectstmt;
  
    if(sqlite3_prepare_v2(database, getOrderList, -1, &selectstmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(selectstmt) == SQLITE_ROW) {
            
            NSInteger primaryKey = sqlite3_column_int(selectstmt, 0);
    
            if (sqlite3_open([[self dataFilePath] UTF8String], &database)!= SQLITE_OK) {
            
                sqlite3_close(database);
                NSAssert(0, @"Failed to open database");
            }
        }   
    }

    
    char *errorMsg;
        
    NSString *addOrder1 = @"INSERT INTO MyOrder (OrderDate,OrderID,State,Comment) VALUES (\"datetime(now)\",NULL,\"false\",\"TestKommentar\")";
        
    NSString *addOrder2 = @"INSERT INTO MyOrder VALUES (datetime(now),false,\"TestKommentar\")";
        
    NSString *addOrder3 = @"INSERT INTO MyOrder VALUES (datetime(now),false,\"TestKommentar\")";
    
    if (sqlite3_exec(database, [addOrder1 UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"Failed to create table: %s", errorMsg);
    }
        
}

-(void) writeAllData:(FROSTOrderDataController *)orderController{
   
}
   
-(void) dropAllData{
      sqlite3 *database;
      char *errorMsg;
      
      if (sqlite3_open([[self dataFilePath] UTF8String], &database)
          != SQLITE_OK) {
         
         sqlite3_close(database);
         NSAssert(0, @"Failed to open database");
      }
      
      NSString *deleteMyOrder = @"delete * from MyOrder";
      NSString *deleteMessurement = @"delete * from Messurement";
      
      NSString *deleteCheckPoint = @"delete * from CheckPoint";
      if (sqlite3_exec(database, [deleteMyOrder UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
         sqlite3_close(database);
         NSAssert(0, @"Failed to create table: %s", errorMsg);
      }
      
      if (sqlite3_exec(database, [deleteMessurement UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
         sqlite3_close(database);
         NSAssert(0, @"Failed to create table: %s", errorMsg);
      }
      
      if (sqlite3_exec(database, [deleteCheckPoint UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
         sqlite3_close(database);
         NSAssert(0, @"Failed to create table: %s", errorMsg);
      }
}
   
-(void) readAllData:(FROSTOrderDataController *)orderController{
   
      sqlite3 *database;
      
      if (sqlite3_open([[self dataFilePath] UTF8String], &database)
          != SQLITE_OK) {
         
         sqlite3_close(database);
         NSAssert(0, @"Failed to open database");
      }
      
      NSString *query = @"select from MyOrder order by OrderDate";
      
      sqlite3_stmt *statement;
      if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil)) {
         while (sqlite3_step(statement) == SQLITE_ROW){
            int row = sqlite3_column_int(statement, 0);
            char *rowData = (char *)sqlite3_column_text(statement, 1);

            // data to array
            /*
            orderController addOrderWithID:<#(NSString *)#> comment:<#(NSString *)#> orderState:<#(BOOL)#> date:<#(NSString *)#> checkpoint1:<#(NSString *)#> checkpoint1State:<#(BOOL)#> checkpoint2:<#(NSString *)#> checkpoint2State:<#(BOOL)#> checkpoint3:<#(NSString *)#> checkpoint3State:<#(BOOL)#> measurement1:<#(NSString *)#> measurement1State:<#(BOOL)#> measurement1Unit:<#(NSString *)#> measurement1Value:<#(NSString *)#> measurement2:<#(NSString *)#> measurement2State:<#(BOOL)#> measurement2Unit:<#(NSString *)#> measurement2Value:<#(NSString *)#>
            */
            
         }
         sqlite3_finalize(statement);
      }
      sqlite3_close(database);
}
        
@end